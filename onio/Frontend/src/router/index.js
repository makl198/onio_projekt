import Vue from 'vue'
import Router from 'vue-router'
import bodyProject from '@/components/bodyProject'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'bodyProject',
      component: bodyProject
    }
  ]
})
