var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose'),
    User = require('./schemaModel'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    editUser= require('./editUser'),
    delUser = require('./deleteUser'),
    getUser = require('./getUser'),
    createUser = require('./createUser'),
    listAllUser = require('./listAllUser')

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Userdb'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

app.listen(port);

console.log('user List started on:' + port);

app.route('/users')
    .get(listAllUser.listAllUser)
        .post(createUser.createUser);

app.delete('/users/:userId',(req,res) =>{
    delUser.deleteUser(req.params.userId,res);
});

app.put('/users/:userId',(req,res) =>{
    editUser.put(req,res);
});

app.get('/users/:userId',(req, res) => {
   getUser.getUser(req.params.userId,res);
});
