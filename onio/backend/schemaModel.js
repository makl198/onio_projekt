var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    
    first_name: {
        type: String,
        
    },
    last_name: {
        type: String,
        
    },
    email :{
        type: String,
    },
    phone :{
        type: Number,
    },
    street :{
        type: String,
    },
    city :{
        type: String,
    },
    PSC :{
        type: Number,
    },
   
});

module.exports = mongoose.model('Users', UserSchema);