var mongoose = require('mongoose'),
    User = mongoose.model('Users');
exports.listAllUser = function (req,res) {
    User.find({}, function (err, user) {
        if (err)
            res.send(err);
        else
        	res.json(user);
    });
};