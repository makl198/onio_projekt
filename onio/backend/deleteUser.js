var mongoose = require('mongoose'),
    User = mongoose.model('Users');
exports.deleteUser = function (userId,res) {
	User.findByIdAndRemove(userId, function (err, user) {
	        if (err)
	            res.send(err);
	        else
	          res.json(user);
	    });
}
