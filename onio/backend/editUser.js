var mongoose = require('mongoose'),
    User = mongoose.model('Users');

exports.put = function (req,res) {
   User.findByIdAndUpdate(req.params.userId, req.body, {new: true},function(err,user){
        if (err)
          res.send(err);
        else
         res.json(user);
    });
}
