var mongoose = require('mongoose'),
    User = mongoose.model('Users');

exports.getUser = function (userId,res) {
  User.findById(userId, function (err, user) {
        if (err)
            res.send(err);
        else
        	res.json(user);
    });
}
